//---------------------------------------------------------------------------
/* TLBO SCP Solver
   This program is to apply TLBO to solve Set-Covering Problems.
   
   Author: Dr. Yun Lu,   
		   Assoicate Professor of Mathematic Department
		   lu@kuztown.edu
                      
   @All rights reserved
    Kutztown University, PA, U.S.A
*/
//---------------------------------------------------------------------------

#ifndef _INC_SCPLowLevel_
#define _INC_SCPLowLevel_
#include <time.h>
#include <stdio.h>
//#include <vector>
// random_shuffle example
#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand


using namespace std;

class DataException {};
typedef struct
{
    /*Basic Parameter*/
    int MaxIT;          //Max. no. of iterations for Meta-RaPS
    int Priority;       //%Priority
    
    int GreedyType;     //Which construction heuristic to use
    int ImproveType;    //Which improvement algorithms to use

    int num_solution;  // number of initial solutions for tlbo
    int num_generation; // number of generations in tlbo
    /*Improvement Parameters*/
    int ImpPriority;    //%priority for improvement
    
    int ImpMagnitude;   //%Search Magnitude
    int ImpMaxIT;       //Max. no. of iterations for improvement
    
    int row_cover_threshold;

    bool DefineCoreProblem; //Whether to define a core problem or not.
    bool Construction;      //For construction of improvement

} SCPParameters;

class SCPDataSet
{
public:
    int NoOfRows, NoOfCols;
    int SumOfCost;       //The sum of costs for all columns
    float Density;         //The density of the matrix
    int *FE_List;        //The list of feasible columns
    int FE_Length;       //The length of feasible columns;
	int row_cover_threshold; 
	vector<pair<long, long> > vp;
	
public:
    typedef struct _RowData
    {
        int NoCover;     //No. of Covers in each row
        int* Covers;     //Covers in each row
    } RowData;
    typedef struct _ColData
    {
        int Cost;        //Cost of each column
        int NoEntry;     //No. of entries in each column
        int* Entries;    //The list of entries that could be covered by this column
        int SelTimes;    //The no of times a column is selected
        bool Dominated;  //Is this column dominated by other columns
        bool Included;   //Is this column included in the solution
        bool Candidate;  //The no of time a column is a candidate
        float Penalty;   //The penalty set to the column
        int PenTimes;    //The no of times a column is penalized
        float Utility;   //The evaluation of the contributions of a column
    } ColData;
    RowData *data_Rows;
    ColData *data_Cols;
    int NoIncCols;      //The no of included cols;
    int *IncCols;       //The list of included cols;
    float StartTime, EndTime, BestTime;
public:
    SCPDataSet(FILE *SourceFile);
	void setRowCoverThreshold(int RCT);
    bool Exchangable(int oldCol, int newCol);
    bool Exchangable(int oldCol, int newCol1, int newCol2);
	void SetSCKPIncCover();
	void prnColCost();
	void prnRowCovNum();
	void SortRowByCover();
	void prnSortRowByCover();
    ~SCPDataSet();
protected:
    void InitColEntryData();
    void CheckDomination();
    void SetRowCoverID(int Row, int Index, int CoverID, bool UpdateEntry = true);
};

#define GetColNoEntry(C) data_Cols[C].NoEntry
#define GetColEntryID(C, Index) data_Cols[C].Entries[Index]
#define SetColCost(C, Val) data_Cols[C].Cost = Val
#define GetColCost(C) data_Cols[C].Cost

#define IncColSelTimes(C) data_Cols[C].SelTimes++
#define GetColSelTimes(C) data_Cols[C].SelTimes

#define IncColPenTimes(C) data_Cols[C].PenTimes++
#define GetColPenTimes(C) data_Cols[C].PenTimes
#define SetColPenalty(C, Val) data_Cols[C].Penalty = Val
#define GetColPenalty(C) data_Cols[C].Penalty
#define SetColUtility(C, Val) data_Cols[C].Utility = Val
#define GetColUtility(C) data_Cols[C].Utility

#define SetColCandidate(C) data_Cols[C].Candidate = true;
#define GetColCandidate(C) data_Cols[C].Candidate

#define SetColDominated(C) data_Cols[C].Dominated = true;
#define GetColDominated(C) data_Cols[C].Dominated

#define SetColIncluded(C) data_Cols[C].Included = true;
#define GetColIncluded(C) data_Cols[C].Included


#define SetRowCoverNo(R, No) data_Rows[R].NoCover = No
#define GetRowCoverNo(R) data_Rows[R].NoCover
#define GetRowCoverID(R, Index) data_Rows[R].Covers[Index]

class SCPSolutionSet
{
public:
    int Cost;              //Cost of the solution
    int Cover;             //No of covers in the solution
    int *FE_List;        //The list of feasible columns
    int FE_Length;       //The length of feasible columns;
    int NoOfRows, NoOfCols;
	int row_cover_threshold;
    int sid;            // solution Index
    bool PrimeSoln,RCT;
    SCPDataSet* DataSet;   //The referred dataset for this solution
	std::vector<int> randRowVector;
	
public:
    typedef struct _RowSol
    {
        int  NoSelected;   //No. of selected covers in each row
		bool IsKCovered;    //Is this row covered equal or greater than K times?
    } RowSol;
    RowSol* sol_Rows;

    typedef struct _ColSol
    {
        int  NoUncovered;//No. of uncovered entries if excluding this column
        bool Selected;   //Is this column selected?
		bool Included;   // this column is required in the feasible solution
        int  Redudancy;  //Is this column redudant?
        float Score;  //The score of this column
        int SolTimes;
    } ColSol;
    ColSol* sol_Cols;
    int* Solution;         //The solution of the problem

public:
    SCPSolutionSet(SCPDataSet *pData, bool ColumnInclusion = true);
    void InitFeasibleList();
    void UpdateFeasibleList();
	void ShuffleRows();
    void ReSet();
    void AddCover(int C, bool CheckRedudancy = false);
    void DeleteCover(int C, bool CheckRedudancy = true);
    void CalculateRedudancy();
	void CalculateMCRedudancy();
    void CalculateSolTimes();
    int GetRowCover(int R);
    bool CheckFeasibility();
    bool CheckSolved();
    bool isIncCover( int colid);
    int GetUncoveredRow();
    int getColSelVal(int colid);
    void ScoreCols();
    void prnCover();
	void prnRowCover();
	void prnColUncover();
    //int GetRowNoSelect( int R) { return sol_Rows[R].NoSelected; }
	//int 
    ~SCPSolutionSet();
    void Duplicate(SCPSolutionSet* s2);
    bool operator==( const SCPSolutionSet & ) const;
};
typedef SCPSolutionSet* PSCPSolutionSet;

#define GetSolution(I) Solution[I]

#define GetRowNoSelect(R) sol_Rows[R].NoSelected
#define SetRowNoSelect(R, No) sol_Rows[R].NoSelected = No
#define GetColUncovered(C) sol_Cols[C].NoUncovered
#define SetColUncovered(C, No) sol_Cols[C].NoUncovered = No

#define GetColSelected(C) sol_Cols[C].Selected
#define SetColSelected(C, S) sol_Cols[C].Selected = S
#define GetColRedudancy(C) sol_Cols[C].Redudancy
#define SetColRedudancy(C, R) sol_Cols[C].Redudancy = R

#define IsColIncluded(C) sol_Cols[C].Included
//#define SetColIncluded(C, S) sol_Cols[C].Included = S


#define GetColScore(C) sol_Cols[C].Score
#define SetColScore(C, R) sol_Cols[C].Score = R

#endif
