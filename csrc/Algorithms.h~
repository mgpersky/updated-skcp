//---------------------------------------------------------------------------
/* TLBO SCP Solver
   This program is to apply TLBO to solve Set-Covering Problems.
   
   Author: Dr. Yun Lu,   
		   Assoicate Professor of Mathematic Department
		   lu@kuztown.edu
                      
   @All rights reserved
    Kutztown University, PA, U.S.A
*/
//---------------------------------------------------------------------------


#ifndef _INC_ALGORITHMSSCP_
#define _INC_ALGORITHMSSCP_

#include "LowLevel.h"
#include <time.h>
#include "math.h"
#include <stdio.h>
#include <stdlib.h>

typedef int ( *GREEDY_ALGO ) (SCPSolutionSet*, SCPParameters*);
typedef int ( *IMP_ALGO) (SCPSolutionSet*, SCPParameters*, SCPSolutionSet*);


#define _DECLARE_GREEDY_ALGORITHMS GREEDY_ALGO GreedyAlgorithms[] = { \
    MinCostPerUncovered, MinCostPerUncovered1, MinCostPerUncovered2,\
    MinCostPerUncovered3, MinCostPerUncovered4, MinCostPerUncovered5,\
    RandomAlgorithms}

#define _DECLARE_IMPROVE_ALGORITHMS IMP_ALGO ImproveAlgorithms[] = {    \
    NULL, NeighborSearchByDepth, NeighborSearchByBreath, Replace2By1, Replace3By2 }


#define _LargeNumber_   100000000

extern int *CandList;
extern GREEDY_ALGO GreedyAlgorithms[];
extern  IMP_ALGO ImproveAlgorithms[];


//Randomdized Greedy Algorithms to select a cover
#define _NoOfInclusionAlgorithms_          9
extern int RandomAlgorithms(SCPSolutionSet* pSol, SCPParameters* P);


extern int InitRandomAlgorithms(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered1(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered2(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered3(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered4(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered5(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered6(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered7(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncovered8(SCPSolutionSet* pSol, SCPParameters* P);

extern int MinCostPerUncovered_Random(SCPSolutionSet* pSol, SCPParameters* P);

// start from row for SKCP
extern int MinCostPerUncoveredByRow(SCPSolutionSet* pSol, SCPParameters* P);
extern int MinCostPerUncoveredByRandRow(SCPSolutionSet* pSol, int rid, SCPParameters* P);
extern void InitConstructionByRow(SCPSolutionSet* pSol, SCPParameters* P);
extern void InitConstructionByRandRow(SCPSolutionSet* pSol, SCPParameters* P);
extern void ConstructionByRow(SCPSolutionSet* pSol, SCPParameters* P);
       
//
// ---end of SKCP


//Improvement Algorithms for a good solution
extern int SearchPrimeCover(SCPSolutionSet *pSol, SCPParameters* P);
extern int removeMCRedudancy(SCPSolutionSet *pSol, SCPParameters* P);

extern int SearchRowCoverThreshold(SCPSolutionSet *pSol, SCPParameters* P);
extern int NeighborSearchByDepth(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best);
extern int NeighborSearchByBreath(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best);
extern int Replace2By1(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best);
extern int Replace3By2(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best);

extern int Random(int Low, int High);
extern float ReadTime();
extern void Construction(SCPSolutionSet* pSol, SCPParameters* P);
extern void InitConstruction(SCPSolutionSet* pSol, SCPParameters* P);


#endif
