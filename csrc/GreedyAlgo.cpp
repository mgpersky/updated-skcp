//---------------------------------------------------------------------------
/* TLBO SCP Solver
   This program is to apply TLBO to solve Set-Covering Problems.
   
   Author: Dr. Yun Lu,   
		   Assoicate Professor of Mathematic Department
		   lu@kuztown.edu
                      
   @All rights reserved
    Kutztown University, PA, U.S.A
*/
//---------------------------------------------------------------------------


#include "Algorithms.h"
//#include <random>

// random_shuffle example
#include <iostream>     // std::cout
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include <chrono>       // std::chrono::system_clock



// Start of SKCP Section 

int MinCostPerUncoveredByCol(SCPSolutionSet* pSol, SCPParameters* P){
  int BestCover = 0;
  float BestValue = 10000;
  for (int cid = 1; cid <= pSol->NoOfCols; cid++){
    //std::cout << std::endl << "column: " << cid << " scores: " << pSol->sol_Cols[cid].Score << " selected? " << pSol->sol_Cols[cid].Selected;
    if(!pSol->sol_Cols[cid].Selected && pSol->sol_Cols[cid].Score > 0 && pSol->sol_Cols[cid].Score < BestValue){
      BestValue = pSol->sol_Cols[cid].Score;
      BestCover = cid;
      std::cout << std::endl << "found col: " << cid;
    }
  }
  
  return BestCover;
}

void ConstructionByRow(SCPSolutionSet* pSol, SCPParameters* P)
{
    int rid;
	int seed = int( 100000000*rand());	
	std::srand ( seed );
	std::random_shuffle ( pSol->randRowVector.begin(), pSol->randRowVector.end() );
	for (int sid = 0; sid < pSol->NoOfRows; sid++)
	{
		rid = pSol->randRowVector[sid];	
		if (pSol->sol_Rows[rid].IsKCovered) continue;
		MinCostPerUncoveredByRandRow(pSol, rid, P);	
	}
}
 

void InitConstructionByRow(SCPSolutionSet* pSol, SCPParameters* P)
{
    GREEDY_ALGO SelectBestCover;
    SelectBestCover = MinCostPerUncoveredByRow;
    int BestCover, SelectedCover;
    while ((BestCover = SelectBestCover(pSol, P)) > 0)
    {
        pSol->DataSet->SetColCandidate(BestCover);
        SelectedCover = BestCover;
        pSol->AddCover(SelectedCover);
    }
    printf("InitByRow:  sid=%4d search price cost: %d\n", pSol->sid,pSol->Cost);
}


void InitConstructionByRandRow(SCPSolutionSet* pSol, SCPParameters* P)
{
     
    int rid;
	pSol->ShuffleRows();
	for (int sid = 0; sid < pSol->NoOfRows; sid++)
	{
		rid = pSol->randRowVector[sid];	
		if (pSol->sol_Rows[rid].IsKCovered) continue;
		MinCostPerUncoveredByRandRow(pSol, rid, P);	
	}
	removeMCRedudancy(pSol,P);  
}

int MinCostPerUncoveredByRandRow(SCPSolutionSet* pSol, int rid, SCPParameters* P)
{
	float BestValue,  CurrentValue;
    int j, cid, BestCover;
    while (!pSol->sol_Rows[rid].IsKCovered) {
		BestCover = 0;
		BestValue = _LargeNumber_;
		for (j=1; j<= pSol->DataSet->data_Rows[rid].NoCover; j++) {
			cid = pSol->DataSet->data_Rows[rid].Covers[j];
			if ( !pSol->GetColSelected(cid) && pSol->GetColUncovered(cid)>0 )
				{
					CurrentValue = (float)(pSol->DataSet->GetColCost(cid))/ pSol->GetColUncovered(cid);
					pSol->SetColScore(cid, CurrentValue);
					if ( CurrentValue < BestValue)
					{
						BestValue = CurrentValue;
						BestCover = cid;
					}
				}
			}
		if (BestCover >0){
			pSol->DataSet->SetColCandidate(BestCover);
			pSol->AddCover(BestCover);
		} else {
			
			printf("# ***Error in finding bestCover for rid=%8d\n", rid);
			for (j=1; j<= pSol->DataSet->data_Rows[rid].NoCover; j++) {
				cid = pSol->DataSet->data_Rows[rid].Covers[j];
				printf("j=%4d cid=%8d colSelectied=%4d coluncovered=%4d \n", j, cid,pSol->GetColSelected(cid),pSol->GetColUncovered(cid)  );
				}
			printf("# ------------- \n");
			break;
		}
	
	}
	return 0;
}

int MinCostPerUncoveredByRow(SCPSolutionSet* pSol, SCPParameters* P)
{
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int rid,j, cid;
    for (int sid = 1; sid <= pSol->NoOfRows; sid++)
	{
		rid  = pSol->DataSet->vp[sid].second;   
        if (pSol->sol_Rows[rid].IsKCovered) continue;
		for (j=1; j<= pSol->DataSet->data_Rows[rid].NoCover; j++) {
			cid = pSol->DataSet->data_Rows[rid].Covers[j];
			if ( !pSol->GetColSelected(cid) && pSol->GetColUncovered(cid)>0 )
			{
			    CurrentValue = (float)(pSol->DataSet->GetColCost(cid))/ pSol->GetColUncovered(cid);
				pSol->SetColScore(cid, CurrentValue);
				if ( CurrentValue < BestValue)
				{
					BestValue = CurrentValue;
					BestCover = cid;
				}
			}
		}
    }
    return BestCover;
}
 

// End of SKCP section
// --------------------------

int Random(int Low, int High)
{
    int n =	((int)((double)rand() / ((double)RAND_MAX+1 ) * (High - Low + 1))) + Low;
    return n;
}



void Construction(SCPSolutionSet* pSol, SCPParameters* P)
{
    GREEDY_ALGO SelectBestCover;
    SelectBestCover = GreedyAlgorithms[P->GreedyType];
    int BestCover, SelectedCover;

    while ((BestCover = SelectBestCover(pSol, P)) > 0)
    {
        pSol->DataSet->SetColCandidate(BestCover);
        SelectedCover = BestCover;
        pSol->AddCover(SelectedCover);
    }
    ////Search Prime Cover
    //if (pSol->PrimeSoln) {
        //pSol->CalculateRedudancy();
        //SearchPrimeCover(pSol, P);
    //}
	if (pSol->RCT) {
		//printf("row cover threshold start\n");
		SearchRowCoverThreshold(pSol, P);
		removeMCRedudancy(pSol,P);
		//printf("row cover threshold end\n");
		}
	

}


void InitConstruction(SCPSolutionSet* pSol, SCPParameters* P)
{
    GREEDY_ALGO SelectBestCover;

    SelectBestCover = InitRandomAlgorithms;
    int BestCover, SelectedCover;

    //pSol->prnColUncover();
    while ((BestCover = SelectBestCover(pSol, P)) > 0)
    {
        pSol->DataSet->SetColCandidate(BestCover);

        //printf("# cost: %4d\n",pSol->Cost);
        SelectedCover = BestCover;
        pSol->AddCover(SelectedCover);
        //printf("# best cover: %4d  cost: %4d\n", BestCover, pSol->Cost);

    }
    //Search Prime Cover
    //if (pSol->PrimeSoln) {
        //pSol->CalculateRedudancy();
        //SearchPrimeCover(pSol, P);
        ////printf("Prime init:  sid=%4d search price cost: %d\n", pSol->sid,pSol->Cost);
    //}
	if (pSol->RCT) {
		//printf("row cover threshold start\n");
		//pSol->prnColUncover();
		//pSol->prnRowCover();
		SearchRowCoverThreshold(pSol, P);
		printf("RCT: rct= %2d sid=%4d search price cost: %d\n", pSol->row_cover_threshold, pSol->sid,pSol->Cost);
		//removeMCRedudancy(pSol,P);
		//printf("RCT(removed redudancy): rct= %2d sid=%4d search price cost: %d\n", pSol->row_cover_threshold, pSol->sid,pSol->Cost);
		pSol->prnCover();		
		//pSol->prnRowCover();
		//printf("row cover threshold end\n");
		}
	

}


int InitRandomAlgorithms(SCPSolutionSet* pSol, SCPParameters* P)
{
    int Rand;
    GREEDY_ALGO Greedy[] =
    {
        MinCostPerUncovered,   //c/k      func 2
        MinCostPerUncovered1,  //c/k^2    func 6
        MinCostPerUncovered2,  //c/klogk  func 5


        MinCostPerUncovered6,    //c/log2(k)  func 3
        MinCostPerUncovered7,    //c/klog2(k) func 4
        MinCostPerUncovered8,    //c^1/2/k^2  func 7

    };
    //int Rand = Random(1, 4);
    if (pSol->sid <=6) {
        Rand = pSol->sid;
    } else {
        Rand = Random(1, 6);
    }
    return Greedy[Rand - 1](pSol, P);
}


int RandomAlgorithms(SCPSolutionSet* pSol, SCPParameters* P)
{
    GREEDY_ALGO Greedy[] =
    {
        MinCostPerUncovered,   //c/k      func 2
        MinCostPerUncovered1,  //c/k^2    func 6
        MinCostPerUncovered2,  //c/klogk  func 5

        MinCostPerUncovered6,    //c/log2(k)  func 3
        MinCostPerUncovered7,    //c/klog2(k) func 4
        MinCostPerUncovered8,    //c^1/2/k^2  func 7

    };

    int Rand = Random(1, 6);
    return Greedy[Rand - 1](pSol, P);
}



int MinCostPerUncovered(SCPSolutionSet* pSol, SCPParameters* P)
{
    //Find the best Cover
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
            CurrentValue = (float)(pSol->DataSet->GetColCost(j))/ pSol->GetColUncovered(j);


            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue < BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered1(SCPSolutionSet* pSol, SCPParameters* P)
{

    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
            CurrentValue = (float)(pSol->DataSet->GetColCost(j))/
                            (pSol->GetColUncovered(j) * pSol->GetColUncovered(j));

            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered2(SCPSolutionSet* pSol, SCPParameters* P)
{
    //Find the best Cover
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
            CurrentValue = (float)(pSol->DataSet->GetColCost(j))/
                            (pSol->GetColUncovered(j)*log(1 + pSol->GetColUncovered(j)));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered3(SCPSolutionSet* pSol, SCPParameters* P)
{

    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
        CurrentValue = (float)sqrt(pSol->DataSet->GetColCost(j))/
                            (pSol->GetColUncovered(j));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered4(SCPSolutionSet* pSol, SCPParameters* P)
{

    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {

        CurrentValue = (float)sqrt(pSol->DataSet->GetColCost(j))/
                            (pSol->GetColUncovered(j));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered5(SCPSolutionSet* pSol, SCPParameters* P)
{
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
        CurrentValue = (float)(pSol->DataSet->GetColCost(j))/
                            sqrt(pSol->GetColUncovered(j));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered6(SCPSolutionSet* pSol, SCPParameters* P)
{
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
            CurrentValue = (float)(pSol->DataSet->GetColCost(j))/
                            log2(1.0 + pSol->GetColUncovered(j));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}

int MinCostPerUncovered7(SCPSolutionSet* pSol, SCPParameters* P)
{
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
            CurrentValue = (float)(pSol->DataSet->GetColCost(j))/
                            (pSol->GetColUncovered(j)*log2(1.0+pSol->GetColUncovered(j)));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}


int MinCostPerUncovered8(SCPSolutionSet* pSol, SCPParameters* P)
{
    int BestCover = 0;
    float BestValue = _LargeNumber_, CurrentValue;
    int j;
    for (int c = 1; c <= pSol->FE_Length; c++)
    {
        j = pSol->FE_List[c];
        if (!pSol->GetColSelected(j) && pSol->GetColUncovered(j)>0)
        {
            CurrentValue = (float)sqrt(pSol->DataSet->GetColCost(j))/
                            (pSol->GetColUncovered(j)*pSol->GetColUncovered(j));
            pSol->SetColScore(j, CurrentValue);
            if ( CurrentValue <= BestValue)
            {
                BestValue = CurrentValue;
                BestCover = j;
            }
        }
    }
    return BestCover;
}


