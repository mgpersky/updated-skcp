//---------------------------------------------------------------------------
/* TLBO SCP Solver
   This program is to apply TLBO to solve Set-Covering Problems.
   
   Author: Dr. Yun Lu,   
		   Assoicate Professor of Mathematic Department
		   lu@kuztown.edu
                      
   @All rights reserved
    Kutztown University, PA, U.S.A
*/
//---------------------------------------------------------------------------


#include "Algorithms.h"
#include "LowLevel.h"


int SearchPrimeCover(SCPSolutionSet *pSol, SCPParameters* P)
{
    int InitialCost = pSol->Cost;
    //printf("spc: %d\n", InitialCost);
    //Find the worst cover
    int WorstCover = 0;
    float WorstValue = 0, CurrentValue;
    int NoCols = pSol->DataSet->NoOfCols;
    for (int j = NoCols; j >= 1; j--) //Delete columns in reverse direction
    {
        if (!pSol->IsColIncluded(j) && pSol->GetColSelected(j) && pSol->GetColRedudancy(j) > 0)
        {
            CurrentValue = (float)pSol->DataSet->GetColCost(j);
            if (CurrentValue > WorstValue)
            {
                WorstValue = CurrentValue;
                WorstCover = j;
            }
        }
    }


    if (WorstCover > 0)
    {
        pSol->DeleteCover(WorstCover, true);
		
        SearchPrimeCover(pSol, P);
        //printf("worst pcost: %d WorstCover=%d\n", pSol->Cost,WorstCover );
    }
    //Update penalization
   // printf("#after spc: cost=%7d, Worstcover=%d\n", pSol->Cost,WorstCover);
    return (pSol->Cost - InitialCost);
}

int removeMCRedudancy(SCPSolutionSet *pSol, SCPParameters* P)
{
    int InitialCost = pSol->Cost;
    //printf("spc: %d\n", InitialCost);
    //Find the worst cover
	pSol->CalculateMCRedudancy();
	//printf("after calculate redundancy\n");
    int WorstCover = 0;
    float WorstValue = 0, CurrentValue;
    int NoCols = pSol->DataSet->NoOfCols;
    for (int j = NoCols; j >= 1; j--) //Delete columns in reverse direction
    {
        if (pSol->GetColSelected(j) && pSol->GetColRedudancy(j) > P->row_cover_threshold)
        {
            CurrentValue = (float)pSol->DataSet->GetColCost(j);
            if (CurrentValue > WorstValue)
            {
                WorstValue = CurrentValue;
                WorstCover = j;
            }
        }
    }


    if (WorstCover > 0)
    {
        pSol->DeleteCover(WorstCover, true);
        removeMCRedudancy(pSol, P);
        //printf("worst pcost: %d WorstCover=%d\n", pSol->Cost,WorstCover );
    }
    //Update penalization
   // printf("#after spc: cost=%7d, Worstcover=%d\n", pSol->Cost,WorstCover);
    return (pSol->Cost - InitialCost);
}



int SearchRowCoverThreshold(SCPSolutionSet *pSol, SCPParameters* P)
{
    //int InitialCost = pSol->Cost;
	int row_cover_no, cid; 
    //printf("spc: %d\n", InitialCost);
    //Find the worst cover
    int BestCover = 0;
    float BestValue = 999999999999999999.0, CurrentValue=0;
	int row_cover_threshold = P->row_cover_threshold;
	//printf(" row_cover_threold= %4d\n", row_cover_threshold);
	//printf(" num_of_rows= %4d \n", pSol->NoOfRows);
	int cnt=0;
	for(int rid=1; rid<=pSol->NoOfRows; rid++){
		//printf(" rid= %4d  no_selected= %4d row_threshold= %4d \n", rid, pSol->GetRowNoSelect(rid), row_cover_threshold);
		if (pSol->GetRowNoSelect(rid) < row_cover_threshold){
			cnt++;
			row_cover_no = pSol->DataSet->GetRowCoverNo(rid);
			for(int j = 1; j <= row_cover_no; j++) {
				cid = pSol->DataSet->GetRowCoverID(rid, j);
				//printf("row_id=%4d  col_id= %4d \n", rid, cid);
				if (!pSol->GetColSelected(cid) && pSol->GetColUncovered(cid)> 0 ){
					//printf("row_id=%4d  col_id= %4d  col_cost= %4d uncover=%4d \n", rid, cid, pSol->DataSet->GetColCost(cid),pSol->GetColUncovered(cid) );
				
					CurrentValue = (float)pSol->DataSet->GetColCost(cid)/pSol->GetColUncovered(cid);
					if (CurrentValue < BestValue)
					{
						BestValue = CurrentValue;
						BestCover = cid;
					}
			
				}
				
			}
			//printf("row_id=%4d  no_selected= %4d col_id= %4d  Best_cover=%4d \n", rid, pSol->GetRowNoSelect(rid), cid, BestCover);
		}	
	}
	//printf(" No(rows<rct)=%4d Best_cover= %4d \n", cnt, BestCover);
	if (BestCover > 0)
    {
        pSol->AddCover(BestCover);
        SearchRowCoverThreshold(pSol, P);
        //printf("worst pcost: %d WorstCover=%d\n", pSol->Cost,WorstCover );
    } else {
		
		for(int rid=1; rid<=pSol->NoOfRows; rid++){
			//printf(" rid= %4d  no_selected= %4d row_threshold= %4d \n", rid, pSol->GetRowNoSelect(rid), row_cover_threshold);
			if (pSol->GetRowNoSelect(rid) < row_cover_threshold){
				//printf(" rid= %4d  no_selected= %4d row_threshold= %4d \n", rid, pSol->GetRowNoSelect(rid), row_cover_threshold);
				row_cover_no = pSol->DataSet->GetRowCoverNo(rid);
				for(int j = 1; j <= row_cover_no; j++) {
					cid = pSol->DataSet->GetRowCoverID(rid, j);
					//printf("row_id=%4d  col_id= %4d  col_uncover=%4d col= %4d \n", rid, cid, pSol->GetColUncovered(cid), pSol->GetColSelected(cid));
					if (!pSol->GetColSelected(cid) && pSol->GetColUncovered(cid)> 0 ){
						//printf("row_id=%4d  col_id= %4d  col_cost= %4d uncover=%4d ", rid, cid, pSol->DataSet->GetColCost(cid),pSol->GetColUncovered(cid) );
						CurrentValue = (float)pSol->DataSet->GetColCost(cid)/pSol->GetColUncovered(cid);
						//printf(" ecost= %8.4f \n" , CurrentValue);  	
					}
					
				}
				//printf("row_id=%4d  no_selected= %4d col_id= %4d  Best_cover=%4d \n", rid, pSol->GetRowNoSelect(rid), cid, BestCover);
			}	
		}		
	}
	
	
	return 0;
}


void SearchOneNeighbor(SCPSolutionSet* pIterSol, SCPParameters* P)
{
    int TotalColsToRemove = (pIterSol->Cover) * (P->ImpMagnitude/100.0);

    for (int Count = 0; Count < TotalColsToRemove; Count++)
    {
         int Col = Random(1, pIterSol->Cover);
         if (!pIterSol->DataSet->data_Cols[pIterSol->GetSolution(Col)].Included)
            pIterSol->DeleteCover(pIterSol->GetSolution(Col), true);
    }
    pIterSol->UpdateFeasibleList();
    Construction(pIterSol, P);
}

void SearchOneNeighbor1(SCPSolutionSet* pIterSol, SCPParameters* P)
{
    pIterSol->CalculateSolTimes();
    //Choose columns to delete according to a distribution
    int TotalColsToRemove = pIterSol->Cover * (P->ImpMagnitude/100.0);

    float* CDF = new float[pIterSol->Cover + 2];
    for (int Count = 0; Count < TotalColsToRemove; Count++)
    {
         //Update the CDF
         float TotalP = 0;
         CDF[0] = 0;
         for (int i = 1; i <= pIterSol->Cover; i++)
         {
            int C =  pIterSol->GetSolution(i);
            CDF[i] = (float)pIterSol->DataSet->GetColCost(C)/pIterSol->sol_Cols[C].SolTimes;
            TotalP += CDF[i];
         }
         for (int i = 1; i <= pIterSol->Cover; i++)
            CDF[i] = CDF[i - 1] + CDF[i]/TotalP;

         //Choose a column to delete
         float P0 = (float)Random(1, 1000)/1000.0;
         for (int i = 1; i <= pIterSol->Cover; i++)
         {
            if (P0 > CDF[i - 1] && P0 <= CDF[i])
            {
                pIterSol->DeleteCover(pIterSol->GetSolution(i), true);
                break;
            }
         }
    }
    delete []CDF;
    pIterSol->UpdateFeasibleList();
    Construction(pIterSol, P);
}

int NeighborSearchByDepth(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best)
{
    int InitialCost = pSol->Cost;
    SCPSolutionSet *pNewSol;
    pNewSol = new SCPSolutionSet(pSol->DataSet);
    pNewSol->Duplicate(pSol);

    for (int i = 1; i <= P->ImpMaxIT; i++)
    {
        SearchOneNeighbor(pNewSol, P);

        if (pNewSol->Cost < pSol->Cost)
        {
            pSol->Duplicate(pNewSol);

            if (pSol->Cost < Best->Cost)
            {
                pSol->DataSet->BestTime = ReadTime();
                Best->Duplicate(pSol);
            }
        }
        else
            pNewSol->Duplicate(pSol);
    }
    delete pNewSol;
    return (pSol->Cost - InitialCost);
}

int NeighborSearchByBreath(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best)
{
    int InitialCost = pSol->Cost;
    SCPSolutionSet *pLocalBest = new SCPSolutionSet(pSol->DataSet);
    pLocalBest->Duplicate(pSol);

    for (int i = 1; i <= P->ImpMaxIT; i++)
    {
        SCPSolutionSet *pNewSol = new SCPSolutionSet(pSol->DataSet);
        pNewSol->Duplicate(pSol);
        SearchOneNeighbor(pNewSol, P);
        if (pNewSol->Cost < pLocalBest->Cost)
            pLocalBest->Duplicate(pNewSol);
        delete pNewSol;
    }

    if (pLocalBest->Cost < Best->Cost)
    {
        Best->Duplicate(pLocalBest);
    }
    delete pLocalBest;
    return (pLocalBest->Cost - InitialCost);
}


 

int Replace2By1(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best)
{
    int InitialCost = pSol->Cost;
    for (int i = 1; i <= pSol->Cover - 1; i++)
    {
        int x = pSol->GetSolution(i);
        for (int j = i + 1; j <= pSol->Cover; j++)
        {
            int y = pSol->GetSolution(j);
            //replace x and y with m
            for (int m = 1; m <= pSol->DataSet->NoOfCols; m++)
            {
                if (pSol->DataSet->GetColCost(x)+pSol->DataSet->GetColCost(y)<=
                    pSol->DataSet->GetColCost(m)) continue;
                //decide if m is eligible to replace x and y
                if (pSol->DataSet->Exchangable(x, m) && pSol->DataSet->Exchangable(y, m))
                {
                    pSol->DeleteCover(x);
                    pSol->DeleteCover(y);
                    pSol->AddCover(m);
                }
            }
        }
    }
    //Search Prime Cover
    pSol->CalculateRedudancy();
    SearchPrimeCover(pSol, P);
    if (pSol->Cost < InitialCost)
         Best->Duplicate(pSol);
    return (pSol->Cost - InitialCost);
}

int Replace3By2(SCPSolutionSet* pSol, SCPParameters* P, SCPSolutionSet* Best)
{
    int InitialCost = pSol->Cost;
    for (int i = 1; i <= pSol->Cover - 2; i++)
    {
        int x = pSol->GetSolution(i);
        for (int j = i + 1; j <= pSol->Cover - 1; j++)
        {
            int y = pSol->GetSolution(j);
            for (int k = j + 1; k <= pSol->Cover; k++)
            {
                int z = pSol->GetSolution(k);
                //replace x, y, z with m, n
                for (int m = 1; m <= pSol->DataSet->NoOfCols - 1; m++)
                {
                    for (int n = m + 1; n <= pSol->DataSet->NoOfCols; n++)
                    {
                        if (pSol->DataSet->GetColCost(x)+pSol->DataSet->GetColCost(y)
                            + pSol->DataSet->GetColCost(z)<=
                            pSol->DataSet->GetColCost(m) + pSol->DataSet->GetColCost(n)) continue;
                        //decide if m+n is eligible to replace x, y and z
                        if (pSol->DataSet->Exchangable(x, m, n) && pSol->DataSet->Exchangable(y, m, n)
                            && pSol->DataSet->Exchangable(z, m, n))
                        {
                            pSol->DeleteCover(x);
                            pSol->DeleteCover(y);
                            pSol->DeleteCover(z);
                            pSol->AddCover(m);
                            pSol->AddCover(n);
                        }
                    }
                }
            }
        }
    }
    //Search Prime Cover
    pSol->CalculateRedudancy();
    SearchPrimeCover(pSol, P);
    if (pSol->Cost < InitialCost)
         Best->Duplicate(pSol);
    return (pSol->Cost - InitialCost);
}
