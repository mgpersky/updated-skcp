//---------------------------------------------------------------------------
/* TLBO SCP Solver
   This program is to apply TLBO to solve Set-Covering Problems.
   
   Author: Dr. Yun Lu,   
		   Assoicate Professor of Mathematic Department
		   lu@kuztown.edu
                      
   @All rights reserved
    Kutztown University, PA, U.S.A
*/
//---------------------------------------------------------------------------


#include "LowLevel.h"
#include <iostream>
#include <algorithm>

using namespace std;

SCPDataSet::SCPDataSet(FILE *SourceFile)
{
    data_Rows = NULL; data_Cols = NULL;

    if (SourceFile == NULL) throw DataException();
	else
	{
        //printf("Read file:\n");
        int R, C, Cost;
        fscanf(SourceFile,"%d",&R);    //get the # of rows and columns from the file
        fscanf(SourceFile,"%d",&C);
        NoOfRows = R; NoOfCols = C; SumOfCost = 0;
        cout << "num_of_rows=" << NoOfRows  << " Number of columns= " << NoOfCols << endl; 
        data_Rows = new RowData[R + 1];
        for (int i = 0; i <= R; i++)
        {
            data_Rows[i].NoCover = 0;
            data_Rows[i].Covers  = NULL;
        }

        data_Cols = new ColData[C + 1];
        for (int j = 0; j <= C; j++)
        {
            data_Cols[j].NoEntry = 0;
            data_Cols[j].Entries = NULL;
            data_Cols[j].SelTimes = 0;
            data_Cols[j].Candidate = false;
            data_Cols[j].Dominated = false;
            data_Cols[j].Included  = false;
            data_Cols[j].PenTimes = 0;
            data_Cols[j].Penalty  = 0;
        }

        for(int j = 1;  j <= C; j++) //Loop for getting the cost
        {
            fscanf(SourceFile,"%d",&Cost);
            data_Cols[j].Cost = Cost;
            SumOfCost += Cost;
        }
        FE_List = new int[C + 1];
        FE_Length = 0;
        IncCols = new int[C + 1];
        NoIncCols = 0;

        int CoverNo, CoverID;
        for(int i = 1;  i <= NoOfRows; i++)    //Loop for getting the row data
        {
            if (fscanf(SourceFile,"%d",&CoverNo) == EOF) //No. of Covers for each row
                throw (DataException());
            data_Rows[i].NoCover = CoverNo;
            data_Rows[i].Covers  = new int[CoverNo + 1];
            for(int j = 1; j <= CoverNo; j++)
            {
                if ( fscanf(SourceFile,"%d",&CoverID) == EOF)  //Incomplete Data
                     throw (DataException());

                if (CoverID >= 1 && CoverID <= NoOfCols)
                    SetRowCoverID(i, j, CoverID);
                else
                    throw (DataException());
            }
        }

        InitColEntryData();

        //Verification
        long Sum1 = 0, Sum2 = 0;
        for(int i = 1;  i <= NoOfRows; i++)
            Sum1 += data_Rows[i].NoCover;

        for (int j = 1; j <= NoOfCols; j++)
        {
            Sum2 += data_Cols[j].NoEntry;
        }
        //Incorrect Source File!
        if (Sum1 != Sum2)
            throw (DataException());
        Density = 100.0 * (float)Sum1/(NoOfCols * NoOfRows);
    }

    CheckDomination();
	printf("initial dataset is read\n");
}
void SCPDataSet::prnColCost()
{
    for (int j = 1; j <= NoOfCols; j++)
    {
         printf("%4d %d| ", j, data_Cols[j].Cost);
		 if (j%8==0){
			 printf("\n");
			 }
    }
}

// print out the number of cover columns for each row 
void SCPDataSet::prnRowCovNum()
{
	printf("#*** number of cover columns for each row\n");
    for (int i = 1; i <= NoOfRows; i++)
    {
         printf("%4d %d| ", i, data_Rows[i].NoCover);
		 if (i%8==0){
			 printf("\n");
			 }
    }
}


void SCPDataSet::SetRowCoverID(int Row, int Index, int CoverID, bool UpdateEntry)
{
    data_Rows[Row].Covers[Index] = CoverID;
    //update the no. of entry for each column
    if (UpdateEntry)
        data_Cols[CoverID].NoEntry++;
}

void SCPDataSet::InitColEntryData()
{
    for (int j = 1; j <= NoOfCols; j++)
    {
        //Allocate memory
        int NoEntry = data_Cols[j].NoEntry;
        if (NoEntry > 0)
        {
            data_Cols[j].Entries = new int[NoEntry + 1];
        }
    }
    int* LIndex = new int[NoOfCols + 1];
    for (int i = 1; i <= NoOfCols; i++)
        LIndex[i] = 0;

    for (int i = 1; i <= NoOfRows; i++)
    {
        for (int j = 1; j <= data_Rows[i].NoCover; j++)
        {
            int C = data_Rows[i].Covers[j];
            LIndex[C]++;
            data_Cols[C].Entries[LIndex[C]] = i;
        }
    }
    delete []LIndex;
}

void SCPDataSet::SortRowByCover(){
	// This is a vector of {value,index} pairs
	
	vp.reserve(NoOfRows);
	vp.push_back(make_pair(0,0 )); // dummy first vector 
	for (long i= 1 ;  i<=NoOfRows; i++) {
		vp.push_back(make_pair(data_Rows[i].NoCover,i ));
	}
	// Sorting will put lower values ahead of larger ones,
	// resolving ties using the original index
	sort(vp.begin(), vp.end());
}

void SCPDataSet::prnSortRowByCover(){
	for (long i= 0 ;  i<=NoOfRows; i++) {
		printf("%4d %4d |", vp[i].first, vp[i].second);
		if (i%5==0){printf("\n");}	
	}
	
	
	}


void SCPDataSet::CheckDomination()
{
    //Column Domination
    for (int j = 1; j <= NoOfCols; j++)
    {
        int CombinedCost = 0;
        for (int i = 1; i <= data_Cols[j].NoEntry; i++)
        {
             int R = data_Cols[j].Entries[i];   //The row it cover
             CombinedCost += data_Cols[data_Rows[R].Covers[1]].Cost; //The first cover of the row.
        }
        if (data_Cols[j].Cost > CombinedCost)
            data_Cols[j].Dominated = true;
    }
    //Coumn Inclusion
    for (int i = 1; i <= NoOfRows; i++)
    {
        int DominatedCovers = 0, TheOnlyCover;
        for (int j = 1; j <= data_Rows[i].NoCover; j++)
        {
            if (data_Cols[data_Rows[i].Covers[j]].Dominated) DominatedCovers++;
            else     TheOnlyCover = data_Rows[i].Covers[j];
        }
        if (DominatedCovers == data_Rows[i].NoCover - 1)   //only one cover for this row
        {
            data_Cols[TheOnlyCover].Included = true;
            int k;
            for (k = 1; k <= NoIncCols; k++)
                if (TheOnlyCover == IncCols[k]) break; //The cover has been encountered
            if (k == NoIncCols + 1)
            {
                NoIncCols++;
                IncCols[NoIncCols] = TheOnlyCover;
            }
        }
    }
}

void SCPDataSet::SetSCKPIncCover()
{
	int cid, j;
	for (int i = 1; i <= NoOfRows; i++)
    {
		if (data_Rows[i].NoCover == row_cover_threshold){
			for (j = 1; j <= data_Rows[i].NoCover; j++)
				cid = data_Rows[i].Covers[j];
				NoIncCols++;
				IncCols[NoIncCols] = cid;
				data_Cols[cid].Included = true;	
			}
        
    }
}


bool SCPDataSet::Exchangable(int oldCol, int newCol)
{
    for (int i = 1; i <= data_Cols[oldCol].NoEntry; i++)
    {
        int j;
        for (j = 1; j <= data_Cols[newCol].NoEntry; j++)
        {
            if (data_Cols[oldCol].Entries[i] == data_Cols[newCol].Entries[j])
                break;
        }
        if (j > data_Cols[newCol].NoEntry)
            return false;
    }
    return true;
}

void SCPDataSet::setRowCoverThreshold(int RCT){
	row_cover_threshold = RCT;	
	}
	
	
	
bool SCPDataSet::Exchangable(int oldCol, int newCol1, int newCol2)
{
    for (int i = 1; i <= data_Cols[oldCol].NoEntry; i++)
    {
        int j;
        bool Found = true;
        for (j = 1; j <= data_Cols[newCol1].NoEntry; j++)
        {
            if (data_Cols[oldCol].Entries[i] == data_Cols[newCol1].Entries[j])
                break;
        }
        if (j > data_Cols[newCol1].NoEntry)
            Found = false;

        for (j = 1; j <= data_Cols[newCol2].NoEntry; j++)
        {
            if (data_Cols[oldCol].Entries[i] == data_Cols[newCol2].Entries[j])
                break;
        }
        if (j > data_Cols[newCol2].NoEntry && !Found)
            return false;
    }
    return true;
}

SCPDataSet::~SCPDataSet()
{
    for(int i = 1; i <= NoOfRows; i++)
        if (data_Rows[i].Covers) delete [](data_Rows[i].Covers);
    if (data_Rows) delete []data_Rows;
    for (int j = 1; j <= NoOfCols; j++)
        if (data_Cols[j].Entries) delete [](data_Cols[j].Entries);
    if (data_Cols) delete []data_Cols;
    if (FE_List) delete []FE_List;
    if (IncCols) delete []IncCols;
}

SCPSolutionSet::SCPSolutionSet(SCPDataSet *pData, bool ColumnInclusion)
{
    DataSet = pData;
    NoOfRows = pData->NoOfRows;
    NoOfCols = pData->NoOfCols;
	row_cover_threshold = pData->row_cover_threshold;
    sol_Rows = new RowSol[NoOfRows + 1];
    sol_Cols = new ColSol[NoOfCols + 1];
    Solution = new int[NoOfCols + 1];
    FE_List = new int[NoOfCols + 1];
    PrimeSoln = true;
	RCT = true; // set row cover threshold true
	for (int i=1; i<=NoOfRows; ++i) randRowVector.push_back(i);  // set row index vector for random shuffle
    ReSet();

}

void SCPSolutionSet::ShuffleRows(){
	int seed = int( 100000000*rand());	
	std::srand ( seed );
	std::random_shuffle (randRowVector.begin(), randRowVector.end());	
}
	
	
void SCPSolutionSet::prnCover(){
    printf("# solution %2d cost=%4d  Num_cover=%4d sol(col,redudancy)\n",sid, Cost,Cover);
    for(int it=1;it<=Cover;it++){
		printf("%4d %2d| ", Solution[it], GetColRedudancy(Solution[it]));
		if (it%10==0) {
			printf("\n");
		}
    }
    printf("\n");
}

void SCPSolutionSet::prnRowCover(){
    printf("# solution %2d cost=%4d  Num_cover=%4d\n",sid, Cost,Cover);
    for(int it=1;it<=NoOfRows;it++){
		printf("%4d %3d/%d | ", it, GetRowNoSelect(it),sol_Rows[it].IsKCovered);
		if (it%5==0) {
			printf("\n");
		}
    }
    printf("\n");
}

void SCPSolutionSet::prnColUncover(){
    for(int it=1;it<=NoOfCols;it++){
		printf("%4d %3d %3d | ", it, DataSet->GetColNoEntry(it),GetColUncovered(it));
		if (it%10==0) {
			printf("\n");
		}
    }
    printf("\n");
}



bool SCPSolutionSet::isIncCover(int colid){
    return DataSet->data_Cols[colid].Included;
}


void SCPSolutionSet::ReSet(){
    FE_Length = 0;
    Cost = 0; Cover = 0;
    for (int i = 1; i <= NoOfRows; i++) {
		sol_Rows[i].NoSelected   = 0;
		sol_Rows[i].IsKCovered = false;
		//printf("Reset row: %4d\n", i);
		}

    for (int j = 1; j <= NoOfCols; j++)
    {
        Solution[j] = 0;
        sol_Cols[j].NoUncovered = DataSet->GetColNoEntry(j);
        sol_Cols[j].Selected    = false;
        sol_Cols[j].Redudancy   = 0;
		sol_Cols[j].Included = DataSet->GetColIncluded(j);
    }
    //Add the included columns
    for (int i = 1; i <= DataSet->NoIncCols; i++) {

        AddCover(DataSet->IncCols[i]);
        //printf("# inc column: %4d sol_cost: %4d\n", DataSet->IncCols[i],Cost);
    }
}

void SCPSolutionSet::InitFeasibleList()
{
    FE_Length = 0;
    for (int j = 1; j <= NoOfCols; j++)
    {   //printf("# col: %4d  selected: %d  NoUncovered: %4d   Dominated: %d \n", j, sol_Cols[j].Selected,sol_Cols[j].NoUncovered ,DataSet->data_Cols[j].Dominated  );
        //if (!sol_Cols[j].Selected && sol_Cols[j].NoUncovered > 0 && !DataSet->data_Cols[j].Dominated)
        //{
            FE_Length++;
            FE_List[FE_Length] = j;
			//printf("#fid= %4d  cid=%5d \n", FE_Length, j);
			//}
    }
	//printf("#Feasilbe_length=%4d \n", FE_Length);
}

int SCPSolutionSet::getColSelVal(int colid){
    if (sol_Cols[colid].Selected) {
        return 1;
    }
    return 0;
}
void SCPSolutionSet::UpdateFeasibleList()
{
    FE_Length = 0;
    for (int j = 1; j <= NoOfCols; j++)
    {
        if (!sol_Cols[j].Selected && sol_Cols[j].NoUncovered > 0 && !DataSet->data_Cols[j].Dominated &&
            DataSet->data_Cols[j].Candidate)
        {
            FE_Length++;
            FE_List[DataSet->FE_Length] = j;
        }
    }
}

void SCPSolutionSet::AddCover(int C, bool CheckRedudancy)
{
    //Update the total cost and no. of covers
    Cost += DataSet->GetColCost(C);
    Cover++;
    Solution[Cover] = C;
    sol_Cols[C].Selected = true;
    //update the solution
    int NoEntry =  DataSet->GetColNoEntry(C);
	//int cnt=0;
    for (int i = 1; i <= NoEntry; i++)
    {
        //Set the cover, and update the uncovered entities
        int R = DataSet->GetColEntryID(C, i);
        sol_Rows[R].NoSelected++;
        //Be covered for the first time
        //if (sol_Rows[R].NoSelected == 1)
		if (sol_Rows[R].NoSelected == row_cover_threshold)
        {
            
			//printf("Add col: %4d   rid=%4d row_selected= %4d\n", C, R, sol_Rows[R].NoSelected);            
            int CoverNo = DataSet->GetRowCoverNo(R);
			sol_Rows[R].IsKCovered = true;
            for (int j = 1; j <= CoverNo; j++)
            {
                int AffectedCover = DataSet->GetRowCoverID(R, j);
                sol_Cols[AffectedCover].NoUncovered--;
				//cnt++;
				//if (sol_Cols[AffectedCover].NoUncovered < 0 and cnt <5 ){
					//cnt++;
					//printf("add col %4d: rid=%4d cid=%4d noselected= %4d \n", C, R, AffectedCover, sol_Cols[AffectedCover].NoUncovered);					
				//}
            }
        }
    }
}

void SCPSolutionSet::DeleteCover(int C, bool CheckRedudancy)
{
    //Update the total cost and no. of covers
    Cost -= DataSet->GetColCost(C);

    int PosInSol;
    for (PosInSol = 1; PosInSol <= Cover; PosInSol++)
        if (Solution[PosInSol] == C) break;   //Find the corresponding cover in the solution
    for (int i = PosInSol; i < Cover; i++)
        Solution[i] = Solution[i+1];

    Cover--;
    sol_Cols[C].Selected = false;
    //update the solution
    int NoEntry = DataSet->GetColNoEntry(C);
    for (int i = 1; i <= NoEntry; i++)
    {
        //Set the cover, and update the uncovered entities
        int R = DataSet->GetColEntryID(C, i);
        sol_Rows[R].NoSelected--;
		if (sol_Rows[R].IsKCovered && sol_Rows[R].NoSelected < row_cover_threshold) 
			{ sol_Rows[R].IsKCovered = false;}
		int CoverNo = DataSet->GetRowCoverNo(R);
		for (int j = 1; j <= CoverNo; j++)
        {
            int AffectedCover = DataSet->GetRowCoverID(R, j);
            //Be uncovered for the first time
            if (sol_Rows[R].NoSelected == 0)
            {
                sol_Cols[AffectedCover].NoUncovered++;
            }
            //Update the redudancy
            //Reduandancy is defined as the Cols.Entry[SmallestEntry].NoSelected - 1;
            //if (CheckRedudancy && sol_Cols[AffectedCover].Redudancy >
                //sol_Rows[R].NoSelected - 1)
                //sol_Cols[AffectedCover].Redudancy = sol_Rows[R].NoSelected - 1;
		}
    }
}

void SCPSolutionSet::CalculateRedudancy()
{
    //int NoOfCols = DataSet->NoOfCols;
    for (int j = 1; j <= NoOfCols; j++)
    {
        if (sol_Cols[j].Selected)
        {
            sol_Cols[j].Redudancy   = NoOfCols; //Set to large number
            int NoEntry = DataSet->GetColNoEntry(j);
            for (int k = 1; k <= NoEntry; k++)
            {
                int R = DataSet->GetColEntryID(j, k);
                //Calculate the minimum redudancy of the column
                if (sol_Rows[R].NoSelected < sol_Cols[j].Redudancy + 1)
                    sol_Cols[j].Redudancy = sol_Rows[R].NoSelected - 1;
            }
        }
    }
}

void SCPSolutionSet::CalculateMCRedudancy() // multicovering redudancy 
{
    //int NoOfCols = DataSet->NoOfCols;
    //for (int j = 1; j <= NoOfCols; j++)
	int k,rid, cid, NoEntry, minNoCover;	
	for (int j = 1; j <= Cover; j++)
    {
        cid = Solution[j];
		minNoCover   = NoOfCols; //Set to large number
		NoEntry = DataSet->GetColNoEntry(cid);
		for ( k = 1; k <= NoEntry; k++)
		{
			rid = DataSet->GetColEntryID(cid, k);
			//Calculate the minimum redudancy of the column
			if (sol_Rows[rid].NoSelected < minNoCover)
				minNoCover = sol_Rows[rid].NoSelected;
		}
		sol_Cols[cid].Redudancy = minNoCover;
	}
}



void SCPSolutionSet::CalculateSolTimes()
{
    for (int j = 1; j <= NoOfCols; j++)
        sol_Cols[j].SolTimes = 0;

    for (int R = 1; R <= NoOfRows; R++)
    {
        int CoverNo = DataSet->GetRowCoverNo(R);
        for (int i = 1; i <= CoverNo; i++)
        {
            int C = DataSet->GetRowCoverID(R, i);
            if (sol_Cols[C].Selected)
            {
                sol_Cols[C].SolTimes++;
                break;
            }
        }
    }
}
SCPSolutionSet::~SCPSolutionSet()
{

    if (sol_Rows) delete []sol_Rows;
    if (sol_Cols) delete []sol_Cols;
    if (FE_List) delete []FE_List;
    //if (IncCols) delete []IncCols;
    if (Solution) delete []Solution;
    //if (DataSet->FE_List) delete[] DataSet->FE_List;
    //if (DataSet->IncCols) delete[] DataSet->IncCols;
    //if (Rows) delete []Rows;
    //if (Cols) delete []Cols;
      //if (FE_List) delete []FE_List;
    //if (IncCols) delete []IncCols;

}

bool SCPSolutionSet::CheckFeasibility()
{
    for (int i = 1; i <= NoOfRows; i++)
    {
        int RowCover = GetRowCover(i);
        //if (RowCover  == 0)
		if (RowCover < row_cover_threshold)
            return false;
    }
    return true;
}

bool SCPSolutionSet::CheckSolved(){
  for(int i = 1; i <= NoOfRows; i++){
    if(!sol_Rows[i].IsKCovered){
      return false;
    }
  }
  return true;
}

int SCPSolutionSet::GetUncoveredRow()
{
    for (int i = 1; i <= NoOfRows; i++)
    {
        int RowCover = GetRowCover(i);
        //if (RowCover == 0)
		if (RowCover < row_cover_threshold)
            return i;
    }
    return 0;
}

int SCPSolutionSet::GetRowCover(int R)
{
    int Ret = 0;
    int CoverNo = DataSet->GetRowCoverNo(R);
    for (int i = 1; i <= CoverNo; i++)
    {
        int C = DataSet->GetRowCoverID(R, i);
        if (sol_Cols[C].Selected)
        {
            Ret = C;
            break;
        }
    }
    return Ret;
}

void SCPSolutionSet::ScoreCols(){
  int bestCol = 0;
  int bestScore = 10000;
  for(int i = 1; i <= NoOfCols; i++){
    int cost = DataSet->data_Cols[i].Cost;
    float numRows = 0;
    for(int j = 1; j <= DataSet->data_Cols[i].NoEntry; j++){
      int rowID = DataSet->data_Cols[i].Entries[j];
      if(!sol_Rows[rowID].IsKCovered){
	numRows += 1;
      }
    }
    //std::cout << std::endl << "column: " << i << " costs: " << cost << " and covers: " << numRows << " rows";
    if(numRows > 0){
      float newScore = cost / numRows;
      sol_Cols[i].Score = newScore;
    }else{
      sol_Cols[i].Score = -1;
    }
  }
}

void SCPSolutionSet::Duplicate(SCPSolutionSet* s2)
{
    Cost = s2->Cost;
    Cover = s2->Cover;
    for (int i = 1; i <= s2->Cover; i++)
        Solution[i] = s2->Solution[i];
    int NoRows = s2->NoOfRows;
    for (int i = 1; i <= NoRows; i++)
    {
        sol_Rows[i].NoSelected = s2->sol_Rows[i].NoSelected;
		sol_Rows[i].IsKCovered = s2->sol_Rows[i].IsKCovered;
    }
    int NoCols = s2->NoOfCols;
    for (int j = 1; j <= NoCols; j++)
    {
        sol_Cols[j].NoUncovered = s2->sol_Cols[j].NoUncovered;
        sol_Cols[j].Selected    = s2->sol_Cols[j].Selected;
    }
}

bool SCPSolutionSet::operator==( const SCPSolutionSet & right) const{
  if(Cost != right.Cost){
    return false;
  }
  for(int j = 1; j <= NoOfCols; j++){
    if(sol_Cols[j].Selected != right.sol_Cols[j].Selected){
      return false;
    }
  }
  return true;
}


