//---------------------------------------------------------------------------
/** TLBO SCP Solver
   This program is to apply TLBO to solve Set-Covering Problems.
   
   @author  Dr. Yun Lu,   
   @version 1.0 
   @date 01/28/2018
*/
//---------------------------------------------------------------------------


#include <sys/time.h>
#include <cmath>
#include "tlbo.h"

_DECLARE_GREEDY_ALGORITHMS;
_DECLARE_IMPROVE_ALGORITHMS;

 

TLBOSCP::TLBOSCP(FILE *SourceFile, SCPParameters *_P): P(_P)
{
    try
    {
        pData           = new SCPDataSet(SourceFile);
		//printf("set row threshold in Pdata\n");
		pData->setRowCoverThreshold(P->row_cover_threshold);
		//printf(" init bestSol\n");
        BestSol         = new SCPSolutionSet(pData);
		//printf("set Bestsol\n");
        MeanSol         = new SCPSolutionSet(pData);
		//printf(" after meansol\n");
        MeanSol->PrimeSoln = false;
		MeanSol->RCT = false;
        AllSolutions    = new PSCPSolutionSet[P->num_solution + 1];
        classCols       = new int[pData->NoOfCols+1];
        classMedianCovers = new int[pData->NoOfCols+1];
        //printf("set class is done\n");
        TeacSol = new SCPSolutionSet(pData);
        TempSol = new SCPSolutionSet(pData);
        IMPROVED = false;
        //printf("set TeacSol Temp Sol is done\n"); 
        Improvement     = ImproveAlgorithms[P->ImproveType];
        NUM_NOP = 3;
		//printf(" initial setup is done\n");

        
    }catch (...)
    {
        throw (DataException());
    }
}


void TLBOSCP::Execute()
{
	printf("into excute\n");
	//pData->prnColCost();
	//pData->prnRowCovNum();
	pData->SortRowByCover();
	//pData->prnSortRowByCover();
	//printf("return from excute and did nothing\n");
	//return;
    int num_try,num_gen;
    NoOfImp			= 0;
    NoOfImpTrial    = 0;

    BestSol->Cost   = pData->SumOfCost;

    if (!P->DefineCoreProblem)  //Init all columns as candidate
        for (int j = 1; j <= pData->NoOfCols; j++)
            pData->SetColCandidate(j);

    pData->StartTime = ReadTime();
    setClass();
	prnClass();
	//return;
	printf("\n# TLBO:  max_gen=%4d\n", P->num_generation);
	num_try = 0;
	for (int i=1;i<=P->num_generation;i++){
		IMPROVED = false;
		setTeacher();
		setMeanSol();
		
        teaching();
		//break;
        learning();
		
        if (IMPROVED) {
            num_try = 0;
        } else {
			num_try++;
		}
		
		num_gen = i;
        printf("# generation %4d  median_cost= %4d best_cost=%4d num_try=%4d \n", i, MeanSol->Cost, BestSol->Cost, num_try);
		//if (i > 5 && num_try >= 3) { 
		if (i > 8 && num_try >= 3) { 
			break;
			}
		
		 
	}
    pData->EndTime = ReadTime();
    TotalTime = pData->EndTime - pData->StartTime;
	removeMCRedudancy(BestSol, P);    
    BestSol->prnCover();
	BestSol->prnRowCover();
    printf("\n# TLBO: num_sol=%4d generation=%4d  best_cost=%4d used_time= %8.3f max_gen=%4d\n", P->num_solution,num_gen, BestSol->Cost,TotalTime,P->num_generation);
    printf("# used_time= %8.3f\n", TotalTime);
	//AvgTime   = TotalTime/P->MaxIT;
}

void TLBOSCP::Prime()
{ 
	
    if (!P->DefineCoreProblem)  //Init all columns as candidate
        for (int j = 1; j <= pData->NoOfCols; j++)
            pData->SetColCandidate(j);
	pData->StartTime = ReadTime();
	CurrentSol = new SCPSolutionSet(pData);
	CurrentSol->sid = 1;
	//Init the feasible element list
	CurrentSol->InitFeasibleList();
	P->Construction = true;
	InitConstruction(CurrentSol, P);
	
    pData->EndTime = ReadTime();
    TotalTime = pData->EndTime - pData->StartTime;
    
	CurrentSol->prnCover();
    printf("\n# Prime:func_id=%2d   best_cost=%4d used_time= %8.3f\n", CurrentSol->sid, CurrentSol->Cost,TotalTime);
    printf("# used_time= %8.3f\n", TotalTime);
	 
}

void TLBOSCP::setClass(){
    
    for (int it = 1; it <= P->num_solution; it++)
	{
	    //printf("excute iteration: %d\n", it);
        //Generate a new solution using the randomdized priority rule
        CurrentSol = new SCPSolutionSet(pData);
        CurrentSol->sid = it;
        //Init the feasible element list
        CurrentSol->InitFeasibleList();
        P->Construction = true;
        //InitConstruction(CurrentSol, P);
		//InitConstructionByRow(CurrentSol, P);
		InitConstructionByRandRow(CurrentSol, P);
        AllSolutions[it] = CurrentSol;
	}
}



void TLBOSCP::prnClass(){
    
    for (int it = 1; it <= P->num_solution; it++)
	{
		printf("#*** Class: solution= %4d \n", it);	    
	    AllSolutions[it]->prnRowCover();
		AllSolutions[it]->prnCover();
		printf("#*** Class: end solution= %4d \n", it);
	}
}



void TLBOSCP::setTeacher(){
    int min_cost = _LargeNumber_;
    int best_sol_id = -1;
    // initilize solution pool
	for (int it = 1; it <= P->num_solution; it++)
	{
        CurrentSol = AllSolutions[it];
        if (CurrentSol->Cost < min_cost) {
            best_sol_id = it;
            min_cost = CurrentSol->Cost;
        }
        //printf("it=%4d cost=%4d\n",it, CurrentSol->Cost);
	}
//	printf("Best_sol_id=%4d\n",best_sol_id);
//	printf("Best_cost: %d\n", AllSolutions[best_sol_id]->Cost);
	if (AllSolutions[best_sol_id]->Cost < BestSol->Cost){
        BestSol->Duplicate(AllSolutions[best_sol_id]);
	}
    //printf("# best cost: %d\n", BestSol->Cost);
}

void TLBOSCP::setMeanSol(){
    // initilize solution pool
 
    int sum_cost = 0;
    int max_cover = 0;
    int cover_cutoff;
    for(int it=1; it<=CurrentSol->NoOfCols; it++){
        classCols[it] =0;
        classMedianCovers[it] = 0;
    }

	for (int it = 1; it <= P->num_solution; it++)
	{
        CurrentSol = AllSolutions[it];
        for(int n=1; n<=CurrentSol->Cover; n++){
            classCols[CurrentSol->Solution[n]] += 1;
            sum_cost += pData->GetColCost(CurrentSol->Solution[n]);
            //printf("sol %4d cover=%4d\n",it, CurrentSol->Solution[it]);
        }
        //printf("# sum_cost=%4d  soln_cost=%4d\n", sum_cost, CurrentSol->Cost);
        //break;
	}
	for (int it=1; it <= CurrentSol->NoOfCols; it++) {
        if (classCols[it] > max_cover)  max_cover = classCols[it];
	}
	cover_cutoff = max_cover/2;
	//printf("# max_cover in class: %4d cutoff=%4d\n", max_cover,cover_cutoff);
    MeanSol->ReSet();
    for (int it=1; it <= CurrentSol->NoOfCols; it++) {
        if (classCols[it] > cover_cutoff && !MeanSol->isIncCover(it)) {
            MeanSol->AddCover(it);
        }
    }
    //printf("# median cost: %4d\n", MeanSol->Cost);
    //MeanSol->prnCover();
    //MeanSol->InitFeasibleList();
    
    //printf("# med fel: %4d\n", MeanSol->FE_Length);
    P->Construction = true;
    MeanSol->PrimeSoln = false;
    //Construction(MeanSol,P);
	ConstructionByRow(MeanSol, P);	
	//printf("#*** Mean solution\n");	
	//MeanSol->CalculateMCRedudancy();
	removeMCRedudancy(MeanSol,P); 
	//MeanSol->prnCover();
    //printf("# median cost: %4d\n", MeanSol->Cost);

}

void TLBOSCP::teaching(){
    int colid,T_f, tmp,r; //,pre_cost;
    for (int i=1;i<=P->num_solution;i++){
        CurrentSol = AllSolutions[i];
        TeacSol->ReSet();
        TeacSol->InitFeasibleList();	
        //printf("Teaching: i=%4d,Fe_length=%3d num_cols=%5d \n",i,TeacSol->FE_Length, TeacSol->DataSet->NoOfCols);
        //pre_cost = CurrentSol->Cost;
        for (int j=1;j<=TeacSol->FE_Length;j++){
            colid = TeacSol->FE_List[j];
			if (TeacSol->IsColIncluded(j)){
				 TeacSol->AddCover(colid);
				}
			else {
				 T_f = Random(1,2);
				 r = Random(0,1);
					
				//tmp = int(ceil(CurrentSol->getColSelVal(colid)+ BestSol->getColSelVal(colid)-
				//           T_f * MeanSol->getColSelVal(colid)));
				
				tmp = CurrentSol->getColSelVal(colid) + r * (BestSol->getColSelVal(colid)-
						   T_f * MeanSol->getColSelVal(colid));
				
		   //printf("i=%4d colid=%3d T_f=%2d  tmp=%2d\n",i,colid,T_f, tmp);
				if (tmp>0){
					TeacSol->AddCover(colid);
				}
				
			}
           
        }
        for (int k=1;k<=P->ImpMaxIT;k++){
            TempSol->Duplicate(TeacSol);
            TempSol->InitFeasibleList();
            P->Construction=true;
            //Construction(TempSol,P);
			ConstructionByRow(TempSol,P);
            if (TempSol->Cost < CurrentSol->Cost){
                IMPROVED = true;
                CurrentSol->Duplicate(TempSol);
                if (CurrentSol->Cost < BestSol->Cost) BestSol->Duplicate(CurrentSol);
            }
        }
		//printf("#*** Teaching: solution id= %4d \n", i);
		removeMCRedudancy(BestSol, P);
		//BestSol->prnCover();
		//BestSol->prnRowCover();		
		//printf("# teaching soln %4d: precost= %4d after_cost=%4d\n", i,pre_cost,CurrentSol->Cost);
		//printf("#*** End of Teaching\n");
    }
}

void TLBOSCP::learning(){
    int randid;
    //printf("# before learning: best_cost=%4d\n", BestSol->Cost);
    for (int i=1;i<=P->num_solution;i++){
        while (true) {
            randid = Random(1,P->num_solution);
            if (randid != i) break;
        }
		//printf("Learning solution: %4d\n", i);
		
        CurrentSol = AllSolutions[i];
	    //CurrentSol->prnRowCover();
		//printf("# start learning\n");
        if (CurrentSol->Cost < AllSolutions[randid]->Cost){
            solnL1(CurrentSol, AllSolutions[randid]);
        } else {
            solnL2(CurrentSol, AllSolutions[randid]);
        }

        for (int k=1;k<=P->ImpMaxIT;k++){
            TempSol->Duplicate(TeacSol);
            TempSol->InitFeasibleList();
            P->Construction=true;
            //Construction(TempSol,P);
	        ConstructionByRow(TempSol, P);            
            if (TempSol->Cost < CurrentSol->Cost){
                IMPROVED = true;
                CurrentSol->Duplicate(TempSol);
                if (CurrentSol->Cost < BestSol->Cost) BestSol->Duplicate(CurrentSol);
            }
        }
		//printf("#* after learning\n");
		//CurrentSol->prnRowCover();
		//printf("#** besSol\n");
		//BestSol->prnRowCover();
    }
    //printf("# after learning: best_cost=%4d\n", BestSol->Cost);

}

void TLBOSCP::solnL1(SCPSolutionSet* soln1, SCPSolutionSet* soln2){
//soln2.cost > soln1.cost
    int colid, soln1_j_val,tmp;
    TeacSol->ReSet();
    TeacSol->InitFeasibleList();
    for (int j=1;j<=TeacSol->FE_Length;j++){
        colid = TeacSol->FE_List[j];
		if (TeacSol->IsColIncluded(j)) {
			TeacSol->AddCover(colid);			
			}
		else {
			soln1_j_val = soln1->getColSelVal(colid);
			tmp = ceil( soln1_j_val + Random(0,1) * (soln1_j_val - soln2->getColSelVal(colid)));
			if (tmp > 0.0)  TeacSol->AddCover(colid);
			}
    }
}

void TLBOSCP::solnL2(SCPSolutionSet* soln1, SCPSolutionSet* soln2){
    //soln2.cost < soln1.cost
    int colid, soln1_j_val,tmp;
    TeacSol->ReSet();
    TeacSol->InitFeasibleList();
    for (int j=1;j<=TeacSol->FE_Length;j++){
        colid = TeacSol->FE_List[j];
		if (TeacSol->IsColIncluded(j)) {
			TeacSol->AddCover(colid);			
			}
		else {
        soln1_j_val = soln1->getColSelVal(colid);
        tmp = ceil( soln1_j_val + Random(0,1) * ( soln2->getColSelVal(colid) - soln1_j_val ));
        if (tmp > 0.0) TeacSol->AddCover(colid);
		}
    }
}



TLBOSCP::~TLBOSCP()
{
//    pData           = new SCPDataSet(SourceFile);
//        BestSol         = new SCPSolutionSet(pData);
//        MeanSol         = new SCPSolutionSet(pData);
//        MeanSol->PrimeSoln = false;
//        AllSolutions    = new PSCPSolutionSet[P->num_solution + 1];
//        classCols       = new int[pData->NoOfCols+1];
//        classMedianCovers = new int[pData->NoOfCols+1];
//        AllSolutions[0] = new SCPSolutionSet(pData);
//        TeacSol = new SCPDataSet(pData);
//        TempSol = new SCPDataSet(pData);
	//printf(" in clean SCP\n");
	//printf("#*** Free pData\n");
    if (pData) delete pData;
	//printf("#*** Free BestSol\n");
    
    if (BestSol) delete BestSol;
	//printf("#*** Free Meansol\n");
    	
    if (MeanSol) delete MeanSol;
	//printf("#*** Free classCols\n");
    	
    if (classCols) delete []classCols;
	//printf("#*** Free classMedianCovers\n");
    if (classMedianCovers) delete []classMedianCovers;
	//printf("#*** Free TeacSol\n");
    	
    if (TeacSol) delete TeacSol;
	//printf("#*** Free TempSol\n");
    
    if (TempSol) delete TempSol;
	//printf("cleaned all sols\n");
	//printf("to clean Iterlistcover\n");
    //if (IterListCover) delete []IterListCover;
	//printf("to clean Iterlistcost");    
    //if (IterListCost) delete []IterListCost;
	//printf("cleaned all cover\n");
	//printf("Free Allsolutions\n");
    //for (int it = 0; it <= P->num_solution; it++) {
		//printf("#* Free Allsolutions it=%4d\n", it);
        ////if (AllSolutions[it] != NULL) delete AllSolutions[it];
		////if (AllSolutions[it]) delete AllSolutions[it];
		//}
	//printf("cleaned individual sols\n");    
    printf("# clear Allsolutions\n");
    if (AllSolutions) delete []AllSolutions;
	printf("# out of tlbo class\n");
	//printf("cleaned allsolutions\n");
}


