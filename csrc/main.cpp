/** 
  * @mainpage Mulitset covering problem 
  *
  * @section Install
  *1) make a directory <workdir>, any name you want. 
  *  
  *2) move the skcp.tgz to <workdir>
  
  *3) extract files: tar vxzf skcp.tgz
  * <pre>
  4) compile c/c++ souce codes: 

     a) cd <csrc> directory:

     b) run: make 

       the excutable file "tlbo"will be saved into <workdir/csrc/bin/Release>
   </pre>
  *5) link the "tlbo" to the <workdir> as skcp 
  * 
  * @section Usage

  * How to run(only run skcp in <workdir> directory )

  * ./skcp  orlib/scp41.txt
  *  
  
*/

//---------------------------------------------------------------------------
/**
   @file main.cpp
   @brief This program is the main program to solve MulitiSet-Covering Problem by TLBO.
   
   @author  Dr. Yun Lu,   
   @version 1.0 
   @date 01/28/2018
*/

//---------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include <iostream>
#include "LowLevel.h"
#include "Algorithms.h"
#include "tlbo.h"


using namespace std;
int *CandList;



// Run TBLO 
void Run( const char* filename, SCPParameters* P)
{
  FILE *fp = NULL;
  TLBOSCP *SCP = NULL;
  CandList = NULL;
  fp = fopen(filename,"r");
  SCP = new TLBOSCP(fp, P);
  //Candlist is not used so far in the mulitset covering problem. This is the legacy variable from set covering problem.	
  CandList = new int[max(SCP->pData->NoOfRows, SCP->pData->NoOfCols)+1];
  SCP->Execute();
  if (fp) fclose(fp);
  if (CandList) delete []CandList;
  if (SCP) delete SCP;
}

int calcMaxK(SCPDataSet* data){
  int fewestCovers = data->vp[1].first;
  return fewestCovers;
}

int calcMedK(SCPDataSet* data){
  int max = calcMaxK(data);
  float result = ((max+2)/2) + (((max + 2) % 2) != 0);
  return result;
}

void colOutput(SCPSolutionSet* solution){
  int total = 0;
  for(int j = 1; j < solution->DataSet->NoOfCols; j++){
    //std::cout << "Column " << j << ": selected? " << solution->GetColSelected(j) << " costs: " << solution->DataSet->GetColCost(j);
    if(solution->GetColSelected(j)){
      std::cout << "Column " << j << ": selected? " << solution->GetColSelected(j) << " costs: " << solution->DataSet->GetColCost(j);
      total+= solution->DataSet->GetColCost(j);
      std::cout << " covers rows: ";
      for(int i = 1; i <= solution->DataSet->data_Cols[j].NoEntry; i++){
	std::cout << solution->DataSet->data_Cols[j].Entries[i] << ", ";
      }
      std::cout << std::endl;
    }
    //std::cout << std::endl;
  }
  std::cout << std::endl << "cost: " << total << std::endl;
}

void rowOutput(SCPSolutionSet* solution){
  for(int j = 1; j <= solution->DataSet->NoOfRows; j++){
    std::cout << "Row " << j << ": K covered? " << solution->sol_Rows[j].IsKCovered << " has " << solution->sol_Rows[j].NoSelected << " by columns: ";
    for(int i = 1; i <= solution->DataSet->data_Rows[j].NoCover; i++){
      int colID = solution->DataSet->data_Rows[j].Covers[i];
      if(solution->GetColSelected(colID)){
	std::cout << colID << ", ";
      }
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

int RunRowGreedy( const char* filename, SCPParameters* P, int& improvement, bool printTables) {
  std::cout << "into RunRowGreedy" << std::endl;
  //initial setup
  FILE *fp = NULL;
  //TLBOSCP *SCP = NULL;
  //CandList = NULL;
  fp = fopen(filename,"r");
  SCPDataSet* data = new SCPDataSet(fp);
  data->setRowCoverThreshold(P->row_cover_threshold);
  SCPSolutionSet* greedySol = new SCPSolutionSet(data, true);
  greedySol->ReSet();
  data->SortRowByCover();
  //Med or Max K detection
  if(P->row_cover_threshold == -1){
    data->setRowCoverThreshold(calcMaxK(data));
    greedySol->row_cover_threshold = calcMaxK(data);
    P->row_cover_threshold = calcMaxK(data);
  }
  if(P->row_cover_threshold == -2){
    data->setRowCoverThreshold(calcMedK(data));
    greedySol->row_cover_threshold = calcMedK(data);
    P->row_cover_threshold = calcMedK(data);
  }
  std::cout << "K = " << data->row_cover_threshold << std::endl;
  //row selection
  int rowID = 0;
  for(int loopID = 1; loopID < greedySol->NoOfRows; loopID++){
    rowID = data->vp[loopID].second;
    std::cout << "selected row: " << rowID << " K covered? " << greedySol->sol_Rows[rowID].IsKCovered << " num covers: " << greedySol->sol_Rows[rowID].NoSelected << std::endl;
    if(!greedySol->sol_Rows[rowID].IsKCovered){
      std::cout << "row: " << rowID << " selected" << std::endl;
      MinCostPerUncoveredByRandRow(greedySol, rowID, P);
    }
  }// end for
  std::cout << std::endl << "solution cost before redundancy check: " << greedySol->Cost << std::endl << std::endl;
  //redundancy removal
  improvement = removeMCRedudancy(greedySol, P);
  /*for(int j = 1; j < data->NoOfCols; j++){
    std::cout << "Column " << j << ": selected? " << greedySol->GetColSelected(j) << " costs: " << data->GetColCost(j) << std::endl;
  }
  std::cout << std::endl;*/
  /*for(int j = 1; j < data->NoOfRows; j++){
    std::cout << "Row " << j << ": K covered? " << greedySol->sol_Rows[j].IsKCovered << " has " << greedySol->sol_Rows[j].NoSelected << std::endl;
  }*/
  if(printTables){
    colOutput(greedySol);
    rowOutput(greedySol);
  }
  std::cout << "K = " << data->row_cover_threshold << std::endl;
  std::cout << std::endl << "solution cost: " << greedySol->Cost << std::endl;
  //Candlist is not used so far in the mulitset covering problem. This is the legacy variable from set covering problem.  
  //CandList = new int[max(SCP->pData->NoOfRows, SCP->pData->NoOfCols)+1];
  if (fp) fclose(fp);
  return greedySol->Cost;
  //if (CandList) delete []CandList;
  //if (SCP) delete SCP;
}

int RunColGreedy(const char* filename, SCPParameters* P, int& improvement, bool printTables){
  std::cout << std::endl << "into RunColGreedy" << std::endl;
  //initial setup
  FILE *fp = NULL;
  //TLBOSCP *SCP = NULL;
  //CandList = NULL;
  fp = fopen(filename,"r");
  SCPDataSet* data = new SCPDataSet(fp);
  data->setRowCoverThreshold(P->row_cover_threshold);
  SCPSolutionSet* greedySol = new SCPSolutionSet(data, true);
  greedySol->ReSet();
  data->SortRowByCover();
  /*greedySol->ScoreCols();
  for(int i = 1; i < greedySol->NoOfCols; i++){
    std::cout << std::endl << "column " << i << " scores: " << greedySol->sol_Cols[i].Score;
    }
*/
  //bool solved = greedySol->CheckFeasibility();
  if(P->row_cover_threshold == -1){
    data->setRowCoverThreshold(calcMaxK(data));
    greedySol->row_cover_threshold = calcMaxK(data);
    P->row_cover_threshold = calcMaxK(data);
  }
  if(P->row_cover_threshold ==- 2){
    data->setRowCoverThreshold(calcMedK(data));
    greedySol->row_cover_threshold = calcMedK(data);
    P->row_cover_threshold = calcMedK(data);
  }
  std::cout << std::endl << "starting" << std::endl;
  //int testThing = 1001;
  while(!greedySol->CheckSolved() /*&& testThing > 0*/){
    greedySol->ScoreCols();
    int cid = MinCostPerUncoveredByCol(greedySol, P);
    std::cout << std::endl << "column: " << cid << " selected";
    greedySol->AddCover(cid);
    //testThing--;
    //solved = greedySol->CheckFeasibility();
  }
  colOutput(greedySol);
  improvement = removeMCRedudancy(greedySol, P);
  if(printTables){
    colOutput(greedySol);
    rowOutput(greedySol);
  }
  std::cout << "K = " << data->row_cover_threshold << std::endl;
  std::cout << std::endl << "solution cost: " << greedySol->Cost << std::endl << "improvement: " << improvement << std::endl;
  if (fp) fclose(fp);
  return greedySol->Cost;
}

// set some parameters for TLBO, such as num_solution, num_generation
void ReadParameters(SCPParameters &Para)
{
    
    Para.GreedyType  = 6;
	Para.ImproveType = 6;
    
    Para.row_cover_threshold = 2;
    
    Para.num_generation = 600;
    Para.num_solution = 30;
	
    //Improvement Parameters
    Para.ImpMaxIT = 10;
    
}

//return time in seconds 
float ReadTime()
{
    struct tm * t;
    time_t timer;
    time(&timer);
    t = localtime(&timer);
    //printf("thour=%4d\n", t->tm_hour );
    float fCurrent = (float)t->tm_hour * 3600.0 + (float)t->tm_min * 60.0 +
            (float)t->tm_sec;
	//printf("# current time: %7.1f\n", fCurrent);
    return fCurrent;
}

void fullLibrary(SCPParameters* P, int greedyType){
  const char *  files[]={"scp41.txt", "scp42.txt", "scp43.txt", "scp44.txt", "scp45.txt", 
			 "scp46.txt", "scp47.txt", "scp48.txt", "scp49.txt", "scp410.txt", 
			 "scp51.txt", "scp52.txt", "scp53.txt", "scp54.txt", "scp55.txt", 
			 "scp56.txt", "scp57.txt", "scp58.txt", "scp59.txt", "scp510.txt", 
			 "scp61.txt", "scp62.txt", "scp63.txt", "scp64.txt", "scp65.txt",
			 "scpa1.txt", "scpa2.txt", "scpa3.txt", "scpa4.txt", "scpa5.txt",
			 "scpb1.txt", "scpb2.txt", "scpb3.txt", "scpb4.txt", "scpb5.txt",
			 "scpc1.txt", "scpc2.txt", "scpc3.txt", "scpc4.txt", "scpc5.txt",
			 "scpd1.txt", "scpd2.txt", "scpd3.txt", "scpd4.txt", "scpd5.txt",
  };
  int costs[45];
  int improvements[45];
  for(int i =0; i < 45; i++){
    int improvement = 0;
    if(greedyType == 1){
      int cost = RunRowGreedy(files[i], P, improvement, false);
      costs[i] = cost;
      improvements[i] = improvement;
    }else if(greedyType == 2){
      int cost = RunColGreedy(files[i], P, improvement, false);
      costs[i] = cost;
      improvements[i] = improvement;
    }
  }
  for(int i = 0; i < 45; i++){
    std::cout << std::endl << "file: " << files[i] << " costs: " << costs[i] << " and improved: " << improvements[i] << " after redundency check";
  }
}

SCPSolutionSet* GenRandomSol( const char* filename, SCPParameters* P) {
  //initial setup
  FILE *fp = NULL;
  //TLBOSCP *SCP = NULL;
  //CandList = NULL;
  fp = fopen(filename,"r");
  SCPDataSet* data = new SCPDataSet(fp);
  data->setRowCoverThreshold(P->row_cover_threshold);
  SCPSolutionSet* greedySol = new SCPSolutionSet(data, true);
  greedySol->ReSet();
  data->SortRowByCover();
  greedySol->ShuffleRows();
  //Med or Max K detection
  if(P->row_cover_threshold == -1){
    data->setRowCoverThreshold(calcMaxK(data));
    greedySol->row_cover_threshold = calcMaxK(data);
    P->row_cover_threshold = calcMaxK(data);
  }
  if(P->row_cover_threshold == -2){
    data->setRowCoverThreshold(calcMedK(data));
    greedySol->row_cover_threshold = calcMedK(data);
    P->row_cover_threshold = calcMedK(data);
  }
  //row selection
  int rowID = 0;
  for(int loopID = 1; loopID < greedySol->NoOfRows; loopID++){
    rowID = greedySol->randRowVector[loopID];
    if(!greedySol->sol_Rows[rowID].IsKCovered){
      MinCostPerUncoveredByRandRow(greedySol, rowID, P);
    }
  }// end for
  //redundancy removal
  int improvement = removeMCRedudancy(greedySol, P);
  //Candlist is not used so far in the mulitset covering problem. This is the legacy variable from set covering problem.
  //CandList = new int[max(SCP->pData->NoOfRows, SCP->pData->NoOfCols)+1];
  if (fp){
    fclose(fp);
  }
  std::cout << std::endl << greedySol->Cost << " a" << std::endl;
  return greedySol;
  //if (CandList) delete []CandList;
  //if (SCP) delete SCP;
}

PSCPSolutionSet PopGen(const char* scp_file_name, SCPParameters* P){
  std::cout << std::endl << "generating population" << std::endl;
  PSCPSolutionSet population[P->num_solution + 1];
  std::cout << std::endl << "population initilized";
  SCPSolutionSet* temp;
  SCPSolutionSet* best;
  for(int solID = 1; solID <= P->num_solution; solID++){
    std::cout << std::endl << "generating solution: " << solID;
    temp = GenRandomSol(scp_file_name, P);
    population[solID] = temp;
    for(int checkID = 1; checkID < solID; checkID++){
      std::cout << std::endl << "checking solution " << solID << "against solution " << checkID; 
      if(population[solID] == population[checkID] || !population[solID]->CheckSolved()){
	solID--;
	break;
      }
    }
  }
  best = population[1];
  for(int solID = 1; solID <= P->num_solution; solID++){
    std::cout << std::endl << "solution " << solID << " costs: " << population[solID]->Cost;
    if(population[solID]->Cost < best->Cost){
      best = population[solID];
    }
  }
  std::cout << std::endl;
  std::cout << std::endl << "best solution costs: " << best->Cost << std::endl;
  colOutput(best);
  rowOutput(best);
  std::cout << std::endl << "best solution costs: " << best->Cost << std::endl;
}


// main program for solving multiset covering problem 
int main(int argc, char *argv[])
{
  const char *scp_file_name;
  clock_t  t_start, used_tm;
  int  num_solution = 30, num_k=2;
  double start_time = ReadTime();
  double stop_time, difference;
  int operation = 1;
  PSCPSolutionSet population;

  t_start = clock();
  cout << "SKCP Test" << endl;
  if (argc==2){
    scp_file_name=argv[1];
  }
  else if (argc==3){
    scp_file_name=argv[1];
    num_solution = atoi(argv[2]);
  }
  else if (argc==4){
    scp_file_name=argv[1];
    num_solution = atoi(argv[2]);
    num_k = atoi(argv[3]);
  }
  else if (argc==5){
    scp_file_name=argv[1];
    num_solution = atoi(argv[2]);
    num_k = atoi(argv[3]);
    operation = atoi(argv[4]);
  }
  else {
    scp_file_name="scp41.txt";
  }
  cout <<"# Reading scp file " << scp_file_name <<endl;
  //int seed = time(NULL);
  srand(time(NULL));
  start_time = ReadTime();
  SCPParameters Para;
  ReadParameters(Para);
  /*Para.num_solution = num_solution;
    Para.row_cover_threshold = num_k;
    cout <<"into Run()" << endl;
    Run(scp_file_name, &Para);
    cout <<"out of Run()" << endl;*/
  
  Para.num_solution = num_solution;
  Para.row_cover_threshold = num_k;
  int improvement = 0;
  if(scp_file_name == "fullLibrary"){
    fullLibrary(&Para, operation);
  }else{
    if(operation == 1){
      RunRowGreedy(scp_file_name, &Para, improvement, true);
    }else if (operation == 2){
      RunColGreedy(scp_file_name, &Para, improvement, true);
    }else if (operation == 3){
      population = PopGen(scp_file_name, &Para);
    }
  }
  // calculate used time
  used_tm = clock() - t_start;
  stop_time = ReadTime();
  difference = stop_time - start_time;
  //printf("calculate used time\n");
  printf ("It took me %d clicks (%f seconds): used_time=%7.3f.\n",used_tm,((float)used_tm)/CLOCKS_PER_SEC,difference);
  return 0;
}
