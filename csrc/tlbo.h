//---------------------------------------------------------------------------
/**
   tlbo.h 
   This program is definitions of TLBOSCP class and its variables and functions.
   The TLBOSCP class is the implementation of TBLO algorithm
       
   @author  Dr. Yun Lu,   
   @version 1.0 
   @date 01/28/2018
*/

//---------------------------------------------------------------------------

#ifndef _INC_SCPSPARSE_
#define _INC_SCPSPARSE_

#include "LowLevel.h"
#include "Algorithms.h"
 

class TLBOSCP
{
protected:
    IMP_ALGO Improvement;     //Improvement Algorithm
  //  bool Disp;                //display progress?
public:
    SCPParameters* P;         //Parameters
    SCPDataSet  *pData;       //Data set
    SCPSolutionSet *CurrentSol, *TeacSol, *TempSol, *IterSol, *BestSol,*MeanSol; //Solutions
    PSCPSolutionSet* AllSolutions;
    //Store the iteration result list
    int *classCols;       //The summary of each column in class
    int *classMedianCovers;     //Medean covers in a classroom
    //Store other performance measures
    int NoOfImp, NoOfImpTrial;
    float TotalTime, AvgTime;
    bool IMPROVED;
    int NUM_NOP; // Number of no-progress
public:
	TLBOSCP(FILE *SourceFile, SCPParameters *_P);

	~TLBOSCP();

	void Execute();
	void Prime();
	void NewExe();
	void setClass();
    void prnClass();
	
	void teaching();
	void learning();

    void setTeacher();
    void setMeanSol();
    void solnL1(SCPSolutionSet* soln1, SCPSolutionSet* soln2);
    void solnL2(SCPSolutionSet* soln1, SCPSolutionSet* soln2);
    
};
#endif

